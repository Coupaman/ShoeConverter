//
//  ViewController.swift
//  ShoeConverter
//
//  Created by Neil on 28/10/2014.
//  Copyright (c) 2014 Coupaman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mensShoeSizeTextField: UITextField!
    @IBOutlet weak var mensConvertedShoeSizeLabel: UILabel!
    @IBOutlet weak var womensShoeSizeTextField: UITextField!
    @IBOutlet weak var womensConvertedShoeSizeLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func convertButtonPressed(sender: UIButton) {
        let conversionConstant = 30
        let originalShoeSize = mensShoeSizeTextField.text.toInt()!
        mensConvertedShoeSizeLabel.text = "\(originalShoeSize) is EU size \(originalShoeSize + conversionConstant)"
        mensConvertedShoeSizeLabel.hidden = false

    }

    @IBAction func convertWomensButtonPressed(sender: UIButton) {
        let conversionConstant = 30.5
        let originalShoeSize = Double((womensShoeSizeTextField.text as NSString).doubleValue)
        womensConvertedShoeSizeLabel.text = "\(originalShoeSize) is EU size \(originalShoeSize + conversionConstant)"
        womensConvertedShoeSizeLabel.hidden = false
    }
}

